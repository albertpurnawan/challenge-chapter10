import { Container } from 'react-bootstrap';
import UpcomingList from '../components/UpcomingList';

const Upcoming = () => (
  <Container>
    <h1 className="text-center my-3">Movies</h1>
    <UpcomingList />
  </Container>
);

export default Upcoming;
