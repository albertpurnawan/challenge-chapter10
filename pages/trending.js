import { Container } from 'react-bootstrap';
import TrendingList from '../components/TrendingList';

const Trending = () => (
  <Container>
    <h1 className="text-center my-3">Movies</h1>
    <TrendingList />
  </Container>
);

export default Trending;
