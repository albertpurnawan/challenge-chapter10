import { useContext } from "react";
import { movieContext } from "../context/movies/moviesContext";
import { Col, Container, Row, Image } from "react-bootstrap";
import Collapsed from "./ui/Collapse";
import { StarIcon, ThumbUpIcon } from '@heroicons/react/outline';
import { useRouter } from 'next/router';

const Movie = () => {
  const { movie } = useContext(movieContext);
  const router = useRouter();

  return (
    <Container>
      <button className="text-xl" type="button" onClick={() => router.back()}>
        <h1>GO BACK</h1>
      </button>

      <h1 className="text-center my-5">{movie.original_title}</h1>

      <Row>
        <Col md={4}>
          <Image
            src={`http://image.tmdb.org/t/p/w300${movie.poster_path}`}
            thumbnail
          />
        </Col>
        <Col md={8}>
          <Container>
            <div>
              <p>{movie.overview}</p>
              <p>
                <b>Release Date: </b>
                {movie.release_date}
              </p>
              <p>
                <b>Home Page:</b> {movie.homepage}
              </p>
            </div>
            <div className="flex items-center py-5">
              <StarIcon className="h-6" />
              {movie.vote_average}
              <ThumbUpIcon className="h-6 ml-4" />
              {movie.vote_count}
            </div>
            <Collapsed />
          </Container>
        </Col>
      </Row>
    </Container>
  );
};

export default Movie;
