import { useState, useContext } from "react";
import { useRouter } from "next/router";
import { Form, FormControl, Button } from 'react-bootstrap';
import { movieContext } from '../context/movies/moviesContext';
import { getMovies } from '../helpers/crud';
import HomeItem from './HomeItem';
import TrendingItem from './TrendingItem';
import UpcomingItem from './UpcomingItem';
import {
  FilmIcon,
  HomeIcon,
  StarIcon,
  ArrowCircleUpIcon,
} from '@heroicons/react/outline';

const Header = () => {
  const [search, setSearch] = useState('');
  const { searchMovies } = useContext(movieContext);
  const router = useRouter();

  const handleOnChange = (e) => setSearch(e.target.value);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const seekMovies = await getMovies(`/search/movie?query=${search}`);
      searchMovies(seekMovies.results);
      setSearch('');
      router.push(`/search?movie=${search}`);
    } catch (error) {
      console.log('error.message :>> ', error.message);
    }
  };

  return (
    <header className="flex flex-col sm:flex-row m-5 justify-between items-center h-auto w-auto">
      <div className="flex flex-grow justify-evenly max-w-2xl">
        <HomeItem title="HOME" Icon={HomeIcon} />

        <TrendingItem title="TRENDING" Icon={StarIcon} />

        <UpcomingItem title="UPCOMING" Icon={ArrowCircleUpIcon} />
      </div>
      <Form inline onSubmit={handleSubmit}>
        <FormControl
          type="text"
          placeholder="Search"
          className="mr-sm-2"
          onChange={handleOnChange}
          value={search}
        />
        <Button type="submit" variant="outline-success">
          Search
        </Button>
      </Form>
      <h1 className="text-xl font-black">NONTON KUY</h1>
    </header>
  );
};

export default Header;
