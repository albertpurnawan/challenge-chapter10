import Link from 'next/link';
function UpcomingItem({ Icon, title }) {
  return (
    <div className="flex flex-col items-center cursor-pointer group w-8 sm:w-10 md:w-20 hover:text-white">
      <Icon className="h-6 mb-2 group-hover:animate-ping" />
      <Link href="/upcoming" passHref>
        <p className="opacity-0 group-hover:opacity-100 tracking-widest">
          {title}
        </p>
      </Link>
    </div>
  );
}

export default UpcomingItem;
