import { useContext, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { movieContext } from '../context/movies/moviesContext';
import { Card, Row, Col } from 'react-bootstrap';
import Link from 'next/link';
import { StarIcon, ThumbUpIcon } from '@heroicons/react/outline';

const TrendingList = () => {
  const [showtrending, setShowtrending] = useState([]);
  const { trending, searchMovie } = useContext(movieContext);
  const router = useRouter();

  useEffect(() => {
    const render = () =>
      router.pathname === '/search'
        ? setShowtrending(searchMovie)
        : setShowtrending(trending);
    render();
  }, [router, showtrending, trending]);

  return (
    <Row>
      {showtrending.length !== 0
        ? showtrending.map((x) => (
            <Col
              md={4}
              className="my-3 cursor-pointer group hover:scale-110"
              key={x.id}
            >
              <Link href={`/movies/${x.id}`} passHref>
                <Card>
                  <Card.Img
                    variant="top"
                    src={`http://image.tmdb.org/t/p/w300${x.backdrop_path}`}
                    className="img-thumbnail img-homeList"
                  />

                  <Card.Body>
                    <Card.Title>
                      <h1>{x.original_title}</h1>
                    </Card.Title>
                    <Card.Text>{x.overview.slice(0, 90) + ' ...'}</Card.Text>
                    <Card.Subtitle>
                      Release Date: {x.release_date}
                    </Card.Subtitle>
                    <div className="flex items-center ">
                      <StarIcon className="h-6" />
                      {x.vote_average}
                      <ThumbUpIcon className="h-6 ml-4" />
                      {x.vote_count}
                    </div>
                  </Card.Body>
                </Card>
              </Link>
            </Col>
          ))
        : null}
    </Row>
  );
};

export default TrendingList;
