import { createContext, useEffect, useReducer } from "react";
import moviesReducer from "./moviesReducer";
import { getMovies } from "../../helpers/crud";
import { cleanData } from "../../helpers/cleanData";

export const movieContext = createContext();

export const GET_MOVIES = "GET_MOVIES";
export const GET_MOVIE_BY_ID = "GET_MOVIE_BY_ID";
export const GET_CHARACTERS = "GET_CHARACTERS";
export const SEARCH_MOVIES = "SEARCH_MOVIES";
export const GET_TRENDING = 'GET_TRENDING';
export const GET_UP_COMING = 'GET_UP_COMING';

const MovieWrapper = ({ children }) => {
  const initialState = {
    movies: [],
    movie: {},
    characters: [],
    searchMovie: [],
    trending: [],
    upcoming: [],

  };

  const [state, dispatch] = useReducer(moviesReducer, initialState);

  useEffect(() => {
    const fetchMovies = async () => {
      const movies = await getMovies('/movie/popular');
      if (Object.keys(movies).length !== 0) {
        dispatch({
          type: GET_MOVIES,
          payload: movies.results,
        });
      }
    };
    fetchMovies();
  }, []);

  useEffect(() => {
    const fetchTrending = async () => {
      const trending = await getMovies('/movie/top_rated');
      if (Object.keys(trending).length !== 0) {
        dispatch({
          type: GET_TRENDING,
          payload: trending.results,
        });
      }
    };
    fetchTrending();
  }, []);

  useEffect(() => {
    const fetchUpComing = async () => {
      const upcoming = await getMovies('/movie/upcoming');
      if (Object.keys(upcoming).length !== 0) {
        dispatch({
          type: GET_UP_COMING,
          payload: upcoming.results,
        });
      }
    };
    fetchUpComing();
  }, []);

  const getMovieById = (payload) => {
    dispatch({
      type: GET_MOVIE_BY_ID,
      payload,
    });
  };

  const getCharacters = (payload) => {
    const cleanPayload = cleanData(payload, 'profile_path');
    dispatch({
      type: GET_CHARACTERS,
      payload: cleanPayload,
    });
  };

  const searchMovies = (payload) => {
    const cleanPayload = cleanData(payload, 'backdrop_path');

    dispatch({
      type: SEARCH_MOVIES,
      payload: cleanPayload,
    });
  };

  return (
    <movieContext.Provider
      value={{
        movies: state.movies,
        movie: state.movie,
        characters: state.characters,
        searchMovie: state.searchMovie,
        trending: state.trending,
        upcoming: state.upcoming,
        lastest: state.lastest,
        getMovieById,
        getCharacters,
        searchMovies,
      }}
    >
      {children}
    </movieContext.Provider>
  );
};

export default MovieWrapper;
