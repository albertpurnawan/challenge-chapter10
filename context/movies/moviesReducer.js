import {
  GET_MOVIES,
  GET_CHARACTERS,
  GET_MOVIE_BY_ID,
  SEARCH_MOVIES,
  GET_TRENDING,
  GET_UP_COMING,
} from './moviesContext';

const moviesReducer = (state, action) => {
  switch (action.type) {
    case GET_MOVIES:
      return { ...state, movies: action.payload };
    case GET_MOVIE_BY_ID:
      return { ...state, movie: action.payload };
    case GET_CHARACTERS:
      return { ...state, characters: action.payload };
    case SEARCH_MOVIES:
      return { ...state, searchMovie: action.payload };
    case GET_TRENDING:
      return { ...state, trending: action.payload };
    case GET_UP_COMING:
      return { ...state, upcoming: action.payload };
    default:
      return state;
  }
};

export default moviesReducer;
