import axios from "axios";

const axiosFetch = axios.create({
  baseURL: 'https://api.themoviedb.org/3',
  headers: {
    'Content-Type': 'application/json',
    Authorization:
      'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3NGRjNTE1YjI3M2JlYmU5MzFkYmZhYzA1Yzc4MzdjMiIsInN1YiI6IjYxMzBkMGZlZTM4YmQ4MDA0MzZiYmY2OSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.Hh4dv2LhNdciNuAEd0tbeVqvt6RKtxAK5S2pSWVbzjY',
  },
});

export default axiosFetch;
